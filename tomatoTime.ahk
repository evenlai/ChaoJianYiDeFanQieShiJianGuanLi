#Persistent  ; 让脚本持续运行, 直到用户退出.
    msgInfo = │＼＿╭╭╭╭╭╭╭╭＿／ │   `n│　　　　　　　　　│ 欢迎使用本软件！`n│　　　　　　　　　│ `n│　 ＞　　　　　●　│ 快快开启属于你的番茄时间~`n│≡　     ╰┬┬┬╯    　≡│  `n│　　  　╰—╯    　　│ 一个个小成就积累起来就是了不起的大目标。`n╰—┬ｏ———ｏ┬—╯`n　　│  Tomato  │       `n　　╰┬———┬╯             `n`n快捷键：`n1、win+z 启动番茄时间；2、ctrl+shift+alt+z 清除番茄时间；3、capslock+鼠标左键  移动时间位置；4、win+s 添加工作内容记录；5、ctrl+shif+alt+s 查看已记录的工作内容`n`nPS：`n1、设置开机启动：win+r 开启运行框并输入"shell:startup"   然后将本程序拷贝到启动文件夹中即可。`n2、工作内容保存在当前目录下的log文件夹里面，以周为一个周期。
    ;判断任务记录文件夹是否存在，如果不存在，则创建
    IfNotExist, log
        FileCreateDir, log
    EWD_MouseWin = 0
    STR_TIME_LOG = -
    GUI_X = 1780
    GUI_Y = 1000
    Menu, Tray, NoStandard
    Menu,Tray,add,启动番茄,startTime
    Menu,tray,add,停止番茄,stopTime
    Menu,tray,add,记录工作内容,Save_Todo
    Menu,tray,add,查看工作内容,Show_Todo
    Menu,tray,add
    Menu,tray,add,番茄时钟说明,Show_Info
    Menu, tray, add,本软件帮助说明, MenuHandler  ; 创建新菜单项.
    Menu,tray,add, 退出, MenuClose
    MsgBox, 0, 本软件帮助说明 ,%msgInfo%,10
return
MenuClose:
    ExitApp
return 
MenuHandler:
    MsgBox, 0, 本软件帮助说明, %msgInfo%
return 

Show_Info:
MsgBox,0,番茄时钟说明,　　使用番茄时钟，将番茄时间设为25分钟，专注工作，中途不允许做任何与该任务无关的事，直到番茄时钟响起，然后记录工作内容，短暂休息一下（5分钟就行），每4个番茄时段多休息一会儿。 `n`n原则：`n　　1）一个番茄时间（25分钟）不可分割，不存在半个或一个半番茄时间。`n　　2） 一个番茄时间内如果做与任务无关的事情，则该番茄时间作废。`n　　3）永远不要在非工作时间内使用《番茄时钟》。（例如：用3个番茄时间陪儿子下的棋）`n　　4）不要拿自己的番茄数据与他人的番茄数据比较。`n　　5）番茄的数量不可能决定任务最终的成败。`n　　6）必须有一份适合自己的作息时间表。
return

;打开番茄时间
^+!z:: 
stopTime:
    Reload
return 
#z::
startTime:
    InputBox, SecTime , 番茄时间 , 请输入你的时间，单位（分钟）。PS：(不要超过60分钟)`n`n如果要重新开始，请按(ctrl+shift+alt+z win+z)：,,,,,,,, 25
    ;当点击取消时，不执行。
    if ErrorLevel
        return 
    if( SecTime >= 60 ){
        MsgBox,0,番茄时间错误,输入的时间不能超过60分钟哦。`n`nPS：`n正常情况下，我们很难花1个小时以上专心的做一件事情的。
        gosub,startTime
        return
    }
    ;定义CustomColor的值为EEAA99.EEAA99是颜色.
    CustomColor = EEAA99
    ;AlwaysOnTop总是显示在最高层.LastFound不在标题栏上显示程序名,+Owner不显示在任务栏上.
    Gui +LastFound +AlwaysOnTop -Caption +Owner +ToolWindow 
    ;窗口的颜色为%CustomColor%,%CustomColor%为变量就是上面的EEAA99，但是上一步已经将窗透明,所以这里的颜色
    Gui, Color, %CustomColor%
    ;创建字体,大小为s32.s为Size的意思,s32就是设置文字大小.
    Gui, Font, s32
    ;添加文本内容:番茄时间.字体颜色为cWhite.cWhite是白色的意思.文字颜色还有cRed,cBlue 坐标：x0 y0 长度：w400 高度：h50
    Gui, Add, Text, x0 y0 w400 h50 vMyTime cBlack, 00:00
    ;将颜色%CustomColor%(EEAA99)透明为150.值为0-255
    WinSet, TransColor, %CustomColor% 150
    ;显示窗体
    Gui, Show, x%GUI_X% y%GUI_Y%, 倒计时
    WinGet, EWD_MouseWin, ID, A
    ;计算秒数
    SecTime *= 60
    STR_TIME_LOG = %A_Hour%:%A_Min%:%A_Sec%
    ;定义标签
    MyInertTime:
        ;计算分钟数
        Min := SecTime // 60 
        ;计算秒数
        Sec := mod(SecTime, 60)
        ;统计数据
        if( Min < 10){
            Min = 0%Min%
        }
        if(Sec < 10){
            Sec = 0%Sec%
        }
        ;文本框里显示时间。
        GuiControl,, MyTime, %Min%:%Sec%
        ;休息1秒，
        Sleep,1000
        ;秒数-1
        SecTime -= 1
        ;判断秒数是否小于0 如果不小于0，则跳转到 MyInertTime 标签处。
        if( SecTime >= 0 ) {
            goto MyInertTime
        }else{
            ;如果小于0，则窗体清除，然后响声。
            EWD_MouseWin = 0
            Gui,Destroy
            SoundPlay,%A_ScriptDir%\clock_end.wav
            Sleep,500
            SecTime = 0
            gosub Save_Todo
        }
return 

;拖放倒计时位置逻辑
~MButton & LButton::
CapsLock & LButton::
    ;如果没有倒计时的窗口时，就不做什么操作。
    if( EWD_MouseWin == 0 ){
        return
    }
    ; 切换到屏幕/绝对坐标。
    CoordMode, Mouse  
    ; 获取当前鼠标位置
    MouseGetPos, EWD_MouseStartX, EWD_MouseStartY
    ;指定窗口的位置和大小。  这个窗口是倒计时的窗口。
    WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
    ;定时更新窗口位置
    SetTimer, EWD_WatchMouse, 10
return
EWD_WatchMouse:
    ;检查LButton键盘按键或鼠标/操纵杆按钮是否按下或放开。也可以获取操纵杆的状态。
    GetKeyState, EWD_LButtonState, LButton, P
    ; 按钮已经释放, 所以拖动结束.
    if EWD_LButtonState = U  
    {
        WinGetPos, GUI_X, GUI_Y,,, ahk_id %EWD_MouseWin%
        SetTimer, EWD_WatchMouse, off
        return
    }
    ;检查Escape 键盘按键或鼠标/操纵杆按钮是否按下或放开。也可以获取操纵杆的状态。
    GetKeyState, EWD_EscapeState, Escape, P
    if EWD_EscapeState = D  ; 按下了 Escape, 所以取消拖动.
    {
        WinGetPos, GUI_X, GUI_Y,,, ahk_id %EWD_MouseWin%
        SetTimer, EWD_WatchMouse, off
        WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
        return
    }
    ; 否则, 改变窗口的位置以匹配
    ; 用户拖动鼠标引起的鼠标坐标变化:
    CoordMode, Mouse
    MouseGetPos, EWD_MouseX, EWD_MouseY
    WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
    SetWinDelay, -1   ; 让下面的移动更快速/平滑.
    WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
    EWD_MouseStartX := EWD_MouseX  ; 为下一次调用此子程序进行更新.
    EWD_MouseStartY := EWD_MouseY
return

;记录任务日志。记录你每次番茄的工作内容。
#s::
Save_Todo:
    ;工作内容记录的标题。
    todo_title = 番茄时间结束
    if(STR_TIME_LOG =  "-"){
        todo_title = 工作内容记录
    }
    ;弹窗工作内容记录的弹窗
    InputBox,todo_list,%todo_title%,这次的番茄时间已经结束。 `n请记录这次番茄的工作内容。`n`nPS：`n按 ctrl+shift+alt+s 可以直接查看你记录的工作内容。
    ;如果有输入内容，则记录。
    if ( todo_list ){
        StringRight, Week, A_YWeek, 2
        FileAppend, 【%A_YYYY%-%A_MM%-%A_DD%】【%STR_TIME_LOG% / %A_Hour%:%A_Min%:%A_Sec%】%todo_list%`n, log\%A_YYYY%年第%Week%周任务日志.log
        STR_TIME_LOG = -
        MsgBox,0,%todo_title%,这次工作内容已经记录，请继续接下来的事情。 `n`nPS:`n1、按win+z 继续下个番茄 `n2、按 ctrl+shift+alt+s 可以直接查看你记录的工作内容。
    }else{
        MsgBox,0,%todo_title%,你暂时没有什么可以记录的。 `n`nPS：`n1、按win+z 继续下个番茄时间 `n2、按 ctrl+shift+alt+s 可以直接查看已记录的工作内容。
    }
return 

; 显示任务日志
^+!s::
Show_Todo:
    StringRight, Week, A_YWeek, 2    
    filename = log\%A_YYYY%年第%Week%周任务日志.log
    IfNotExist, %filename%
        FileAppend,,%filename%
    Run %filename%
return 